package com.belatrix.selection;

import com.belatrix.selection.refactor.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author whuaringa on 17/10/2017.
 */
public class JobLoggerImprovedTest {

    LoggerConfig config = new LoggerConfig();

    @Before
    public void setUp() {
        Util.createTable();
    }

    /*SET TEST 1: We want the ability to be able to log to a text file,the console and/or the database.*/
    @Test
    public void choiceMultipleOption() {
        config.addConsole()
                .addTextFile()
                .addDataBase()
                .addTextFile();//validation logger duplicated
        config.reset();
    }

    @Test
    public void choiceMultipleOption2() {
        config.setPrinters(new DataBaseLogger()
                , new ConsoleLogger()
                , new ConsoleLogger());//avoid duplicated
        config.reset();
    }

    /*SET TEST 2: Messages can be marked as*/
    //message,
    @Test
    public void labelMessage() {
        System.out.println(new MessageLogger("Message", LevelLogger.MESSAGE));
    }

    //warning or
    @Test
    public void labelWarning() {
        System.out.println(new MessageLogger("Message", LevelLogger.WARNING));
    }

    //error.
    @Test
    public void labelError() {
        System.out.println(new MessageLogger("Message", LevelLogger.ERROR));
    }

    /*SET TEST 3: We also want the ability to selectively be able to choose what gets logged,
        such as to be able to log only errors or only errors and warnings. */
    @Test
    public void testLogMessage() throws Exception {
        //add or remove level
        config.setLevelLoggers(LevelLogger.MESSAGE, LevelLogger.WARNING, LevelLogger.ERROR);
        //add or remove printer
        config.addConsole()
                .addTextFile()
                .addDataBase();

        config.error("Error ");
        config.message("Message");
        config.warning("Warning");
    }

    @After
    public void tearDown() {
        Util.deleteTable();
    }
}