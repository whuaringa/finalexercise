package com.belatrix.selection.refactor;

import java.util.logging.Level;

/**
 * @author whuaringa on 17/10/2017.
 */

@FunctionalInterface
public interface JobPrinter {

    void writeMessage(MessageLogger message);

    default Level getLevel(MessageLogger message) {
        switch (message.getLevelLogger()) {
            case ERROR:
                return Level.SEVERE;
            case WARNING:
                return Level.WARNING;
            case MESSAGE:
            default:
                return Level.INFO;
        }

    }

}
