package com.belatrix.selection.refactor;

import com.belatrix.selection.db.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

/**
 * @author whuaringa on 17/10/2017.
 */
public class DataBaseLogger implements JobPrinter {

    private String identifier = "DataBase";
    private static final String QUERY = "INSERT INTO log_values values (?,?)";

    @Override
    public void writeMessage(MessageLogger message) {
        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY)) {
            statement.setString(1,message.getText());
            statement.setString(2,String.valueOf(message.getLevelLogger().getCode()));
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        DataBaseLogger that = (DataBaseLogger) o;
        return Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }
}
