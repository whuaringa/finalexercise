package com.belatrix.selection.refactor;

/**
 * @author whuaringa on 18/10/2017.
 */
public class MessageLogger {
    private String text;
    private LevelLogger levelLogger;

    public MessageLogger(String text, LevelLogger levelLogger) {
        this.text = text;
        this.levelLogger = levelLogger;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LevelLogger getLevelLogger() {
        return levelLogger;
    }

    public void setLevelLogger(LevelLogger levelLogger) {
        this.levelLogger = levelLogger;
    }
}
