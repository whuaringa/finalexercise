package com.belatrix.selection.refactor;

import com.belatrix.selection.db.DBConnection;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author whuaringa on 18/10/2017.
 */
public class Util {
    private static final String CREATE_QUERY = "CREATE TABLE LOG_VALUES ( MESSAGE VARCHAR(32000),LEVEL CHAR(1))";
    private static final String DELETE_QUERY = "DROP TABLE LOG_VALUES";

    private Util(){

    }
    public static void createTable() {
        executeDDL(CREATE_QUERY);
    }

    public static void deleteTable(){
        executeDDL(DELETE_QUERY);
    }
    private static void executeDDL(String query) {

        try (Connection connection = DBConnection.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }

}
