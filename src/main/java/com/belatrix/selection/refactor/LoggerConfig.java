package com.belatrix.selection.refactor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author whuaringa on 17/10/2017.
 */


public class LoggerConfig {

    private Set<LevelLogger> levelLoggers = new HashSet<>();
    private Set<JobPrinter> printers = new HashSet<>();

    public void message(String text) {
        write(text, LevelLogger.MESSAGE);
    }

    public void warning(String text) {
        write(text, LevelLogger.WARNING);
    }

    public void error(String text) {
        write(text, LevelLogger.ERROR);
    }

    private void write(String text, LevelLogger levelLogger) {
        MessageLogger message = new MessageLogger(text, levelLogger);
        printers.forEach(
                item -> {
                    if (isConfigured(message)) {
                        item.writeMessage(message);
                    }
                });
    }

    public void setLevelLoggers(LevelLogger... levelLoggers) {
        this.levelLoggers = new HashSet<>(Arrays.asList(levelLoggers));
    }

    public void setPrinters(JobPrinter... printers) {
        this.printers = new HashSet<>(Arrays.asList(printers));
    }

    private boolean isConfigured(MessageLogger message) {
        return levelLoggers.contains(message.getLevelLogger());
    }

    public LoggerConfig addDataBase() {
        printers.add(new DataBaseLogger());
        return this;
    }

    public LoggerConfig addConsole() {
        printers.add(new ConsoleLogger());
        return this;
    }

    public LoggerConfig addTextFile() {
        printers.add(new TextFileLogger());
        return this;
    }

    public void reset(){
        printers = new HashSet<>();
    }
}
