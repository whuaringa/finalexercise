package com.belatrix.selection.refactor;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author whuaringa on 17/10/2017.
 */
public class TextFileLogger implements JobPrinter {

    private String identifier = "DataBase";
    static Logger logger = Logger.getLogger(TextFileLogger.class.getName());
    static {

        try {
            FileHandler fileHandler = new FileHandler("%h/loggerExample2.txt", true);
            fileHandler.setFormatter(new SimpleFormatter());
            logger.setUseParentHandlers(false);
            logger.addHandler(fileHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeMessage(MessageLogger message) {
        logger.log(getLevel(message), message.getText());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        TextFileLogger that = (TextFileLogger) o;
        return Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }


}
