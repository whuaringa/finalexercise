package com.belatrix.selection.refactor;

import java.util.Objects;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author whuaringa on 17/10/2017.
 */
public class ConsoleLogger implements JobPrinter {
    private String identifier = "Console";
    static Logger logger = Logger.getLogger(ConsoleLogger.class.getName());

    static {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(consoleHandler);
    }

    @Override
    public void writeMessage(MessageLogger message) {
        logger.log(getLevel(message), message.getText());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ConsoleLogger that = (ConsoleLogger) o;
        return Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }
}
