package com.belatrix.selection.refactor;

/**
 * @author whuaringa on 17/10/2017.
 */
public enum LevelLogger {

    MESSAGE(1), WARNING(2), ERROR(3);
    private int code;

    LevelLogger(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
