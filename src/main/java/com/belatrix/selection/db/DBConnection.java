package com.belatrix.selection.db;

import javax.security.auth.login.Configuration;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author whuaringa on 18/10/2017.
 */
public class DBConnection {
    private static final String CONFIGURATION_FILE = "/dbconfig.properties";
    private static final Properties properties;

    static {
        properties = new Properties();
        try (InputStream inputStream = Configuration.class.getResourceAsStream(CONFIGURATION_FILE)) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    public static String getValue(String key) {
        return properties.getProperty(key);
    }

    public static Connection getConnection() {

        try {
            return DriverManager.getConnection(getValue("url"), properties);
        } catch (SQLException e) {
            e.printStackTrace(System.out);
        }
        throw new NullPointerException();
    }
}
